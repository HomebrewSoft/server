#!/bin/bash

INSTANCE='local'
DOMAIN='test.homebrewsoft.dev'
ODOO_VERSION='13.0'
POSTGRES_VERSION='12.2'

DOCKERS_DIR='/root/dockers'

REPO_URL='https://gitlab.com/HomebrewSoft/server'
REPO_BRANCH='master'

REMOTE_RESOURCES=$REPO_URL/-/raw/$REPO_BRANCH
NGINX_DIR=$DOCKERS_DIR/nginx
INSTANCE_DIR=$DOCKERS_DIR/$INSTANCE

apt update -y
apt install -y \
    vim \
    docker.io \
    docker-compose \
    git \

systemctl enable docker
systemctl start docker

docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent

mkdir -p $NGINX_DIR
curl $REMOTE_RESOURCES/nginx-proxy-le.yml -o $NGINX_DIR/docker-compose.yml
docker network create nginx-proxy
docker-compose -f $NGINX_DIR/docker-compose.yml up -d

mkdir -p $INSTANCE_DIR
curl $REMOTE_RESOURCES/odoo-postgres.yml -o $INSTANCE_DIR/docker-compose.yml
sed -i "s/\${INSTANCE}/$INSTANCE/g" $INSTANCE_DIR/docker-compose.yml
sed -i "s/\${ODOO_VERSION}/$ODOO_VERSION/g" $INSTANCE_DIR/docker-compose.yml
sed -i "s/\${POSTGRES_VERSION}/$POSTGRES_VERSION/g" $INSTANCE_DIR/docker-compose.yml
sed -i "s/\${DOMAIN}/$DOMAIN/g" $INSTANCE_DIR/docker-compose.yml
sed -i "s/\${CONTACT}/moises@homebrewsoft.dev/g" $INSTANCE_DIR/docker-compose.yml
docker-compose -f $INSTANCE_DIR/docker-compose.yml up -d
ln -s /var/lib/docker/volumes/"$INSTANCE"_backups/_data/ $INSTANCE_DIR/backups
ln -s /var/lib/docker/volumes/"$INSTANCE"_config/_data/ $INSTANCE_DIR/config
ln -s /var/lib/docker/volumes/"$INSTANCE"_db-data/_data/ $INSTANCE_DIR/db-data
ln -s /var/lib/docker/volumes/"$INSTANCE"_extra-addons/_data/ $INSTANCE_DIR/extra-addons
ln -s /var/lib/docker/volumes/"$INSTANCE"_web-data/_data/ $INSTANCE_DIR/web-data
