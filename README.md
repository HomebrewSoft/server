# HomebrewSoft Server Tools

In this repository is contained the `user-data` file that will be used in servers installations of Odoo.

Make sure to adjust the vars in the first lines of the script.

## Steps

1. Create the server
   1. Set the `user-data` field with the content of the file with the same name.

![user-data](doc/img/user-data.png)

   2. Link at least one SSH-Key to the server

![user-data](doc/img/ssh-key.png)

   3. Copy the IP of the server

![user-data](doc/img/ip.png)

1. Link the IP to the domain
    > This can be done creating an `A` record in the DNS provider.

![user-data](doc/img/a-record.png)

3. Create the `endpoint` in the `Portainer` manager
    > Remember to share the access to the client.

![user-data](doc/img/endpoint.png)


* Optional: Create a database and set the `masterpassword`

![user-data](doc/img/masterpasword.png)
