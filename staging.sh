#!/bin/bash

apt update -y
apt upgrade -y

apt install -y \
    vim \
    git \
    zip \
    unzip \
    sudo

apt install -y \
    docker.io \
    docker-compose

systemctl enable docker
systemctl start docker

adduser tester --disabled-password --gecos ""
usermod -aG sudo tester
usermod -aG docker tester

cd /home/tester
git clone https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances/ -b staging
cd odoo-instances
./setup

chown -R tester:tester /home/tester
chown -R tester:tester /dockers
